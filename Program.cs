﻿/*
 * AUTHOR: Armando Martínez 
 * DATE: 2022-01-17
 * DESCRIPTION: Programa que usa la funcion Console.Beep() para reproducir música (tanto la esta función como Thread.Sleep() dependen de la velocidad del procesador, puede haber inconsistencias o algunas notas se pueden saltar al azar)
 */

using System;
using System.Threading;

namespace Music
{
    class Program
    {
        static void Main(string[] args)
        {
            int selectedOption;
            do
            {
                Console.WriteLine("\n[BIENVENIDO, REPRODUCE LA CANCIÓN QUE QUIERAS]\n(se recomienda usar los altavoces)\n");
                selectedOption = Menu();
                if (selectedOption != 0) { Console.Clear(); }
                switch (selectedOption)
                {
                    case 0:
                        break;
                    case 1:
                        Test();
                        break;
                    case 2:
                        Megalovania();
                        break;
                    case 3:
                        Funny();
                        break;
                    case 4:
                        StillAlive();
                        break;
                }
                Console.Clear();

            } while (selectedOption != 0);
            
        }
        static int Menu()
        {
            string[] options = { " 0 - Exit", " 1 - Test", " 2 - Megalovania", " 3 - Funny",
                            " 4 - Still Alive"};
            foreach (string option in options) { Console.WriteLine(option); }
            int selectedOption = ParseIntInRange("\nSELECCIONA UNA OPCIÓ: ", 0, 4);
            return selectedOption;
        }
        static int ParseIntInRange(string prompt, int min, int max)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum > max || parsedNum < min) { Console.WriteLine($"El número ha d'estar entre {min} i {max}!"); }
            } while (!isParsed || parsedNum > max || parsedNum < min);
            return parsedNum;
        }
        static void Test()
        {
            Do(5);
            Re(5);
            Mi(5);
            Fa(5);
            Sol(5);
            La(5);
            Si(5);
            Do(6);
            Console.WriteLine("[ENTER]");
            Console.ReadLine();
        }
        static void Megalovania()
        {
            int o = 5; //octava de la canción
            double t = 400; // tempo
            Re(o, t * 3 / 4);
            Re(o, t * 3 / 4);
            Re(o + 1, t);
            La(o, t * 1.5);
            SolSLaB(o, t);
            Silence(t / 2);
            Sol(o, t);
            Fa(o, t / 2);
            Re(o, t / 2);
            Fa(o, t / 2);
            Sol(o, t / 2);
            Console.WriteLine("[ENTER]");
            Console.ReadLine();
        }
        static void Funny()
        {
            int o = 5; //octava de la canción
            double t = 800; // tempo
            Re(o, t / 4);
            Mi(o, t / 4);
            Sol(o, t / 4);
            Mi(o, t / 4);
            Si(o, t * 3 / 4);
            Si(o, t * 3 / 4);
            La(o, t * 5 / 4);
            Re(o, t / 4);
            Mi(o, t / 4);
            Sol(o, t / 4);
            Mi(o, t / 4);
            La(o, t * 3 / 4);
            La(o, t * 3 / 4);
            Sol(o, t * 3 / 4);
            Mi(o, t / 4);
            Re(o, t / 2);
            Re(o, t / 4);
            Mi(o, t / 4);
            Sol(o, t / 4);
            Mi(o, t / 4);
            Sol(o, t);
            La(o, t / 2);
            FaSSolB(o, t * 3 / 4);
            Mi(o, t / 4);
            Re(o, t / 2);
            Re(o, t / 2);
            Re(o, t / 2);
            La(o, t);
            Sol(o, t * 2.0);
            Console.WriteLine("[ENTER]");
            Console.ReadLine();
        }
        static void StillAlive()
        {
            int o = 5; //octava de la canción
            double t = 400; // tempo
            Console.WriteLine("This was a triumph.");
            Sol(o, t / 2);
            FaSSolB(o, t / 2);
            Mi(o, t / 2);
            Mi(o, t / 2);
            FaSSolB(o, t * 2.0);
            Silence(t * 3.5);
            Console.Write("I'm making a note here: ");
            La(o - 1, t / 4);
            Sol(o, t / 2);
            FaSSolB(o, t / 2);
            Mi(o, t / 2);
            Mi(o, t * 3 / 4);
            FaSSolB(o, t * 1.5);
            Console.WriteLine("\"Huge Success\"");
            Re(o, t);
            Mi(o, t / 2);
            La(o - 1, t * 2.5);
            Silence(t * 1.5);
            Console.WriteLine("It's hard to overstate my satisfaction.");
            La(o - 1, t / 4);
            Re(o, t);
            FaSSolB(o, t / 2);
            Sol(o, t * 1.5);
            Re(o, t / 2);
            DoSReB(o, t * 1.5);
            Re(o, t * 1.5);
            Mi(o, t);
            La(o - 1, t / 2);
            La(o - 1, t);
            FaSSolB(o, t * 1.5);
            Silence(t * 4.0);
            Console.WriteLine("Aperture Science.");
            Sol(o, t / 4);
            FaSSolB(o, t / 4);
            Mi(o, t / 4);
            Mi(o, t / 4);
            FaSSolB(o, t * 2.0);
            Silence(t * 3.5);
            Console.Write("We do what we must, ");
            Mi(o - 1, t / 4);
            Sol(o, t / 4);
            FaSSolB(o, t / 4);
            Mi(o, t / 4);
            Mi(o, t / 2);
            Console.Write("because ");
            FaSSolB(o, t / 2);
            Re(o, t);
            Console.WriteLine("we can.");
            Mi(o, t / 2);
            La(o - 1, t / 2);
            Silence(t * 2.0);
            Console.Write("For the good of all of us, ");
            Mi(o, t);
            FaSSolB(o, t / 2);
            Sol(o, t *  1.5);
            Mi(o, t / 2);
            DoSReB(o, t * 1.5);
            Re(o, t / 2);
            Mi(o,  t);
            Console.WriteLine("except the ones who are dead.");
            La(o - 1, t / 2);
            Re(o, t / 2);
            Mi(o, t / 2);
            Fa(o, t / 2);
            Mi(o, t / 2);
            Re(o, t / 2);
            Do(o, t / 2);
            Silence(t);
            Console.WriteLine("But there's no sense crying over every mistake.");
            La(o - 1, t / 2);
            LaSSiB(o - 1, t / 2);
            Do(o, t);
            Fa(o, t);
            Mi(o, t / 2);
            Re(o, t / 2);
            Re(o, t / 2);
            Do(o, t / 2);
            Re(o, t / 2);
            Do(o, t / 2);
            Do(o, t);
            Do(o, t);
            Console.WriteLine("You just keep on trying 'till you run out of cake.");
            La(o - 1, t / 2);
            LaSSiB(o - 1, t / 2);
            Do(o, t);
            Fa(o, t);
            Sol(o, t / 2);
            Fa(o, t / 2);
            Mi(o, t / 2);
            Re(o, t / 2);
            Re(o, t / 2);
            Mi(o, t / 2);
            Fa(o, t);
            Fa(o, t);
            Console.Write("And the science gets done, ");
            Sol(o, t / 2);
            La(o, t / 2);
            LaSSiB(o, t / 2);
            LaSSiB(o, t / 2);
            La(o, t);
            Sol(o, t);
            Console.WriteLine("and you make a neat gun");
            Fa(o, t / 2);
            Sol(o, t / 2);
            La(o, t / 2);
            La(o, t / 2);
            Sol(o, t);
            Fa(o, t);
            Console.WriteLine("for the people who are still alive.");
            Re(o, t / 2);
            Do(o, t / 2);
            Re(o, t / 2);
            Fa(o, t / 2);
            Fa(o, t / 2);
            Mi(o, t / 2);
            Silence(t / 2);
            Mi(o, t / 2);
            FaSSolB(o, t / 2); 
            FaSSolB(o, t * 2.0);
            Console.ReadLine();

        }
        /* -- -- -- -- NOTAS -- -- -- -- */
        static void Silence(double time = 1000)
        {
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));
        }
        static void Do(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(16.35 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));
        }
        // Do Sostenido || Re Bemol
        static void DoSReB(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(17.32 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void Re(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(18.35 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));
        }
        static void ReSMiB(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(19.45 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));
        }
        static void Mi(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(20.6 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void Fa(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(21.83 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void FaSSolB(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(23.12 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void Sol(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(24.5 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void SolSLaB(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(25.96 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void La(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(27.5 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void LaSSiB(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(29.14 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        static void Si(int octave = 1, double time = 1000)
        {
            Console.Beep(Convert.ToInt32(Math.Round(30.87 * Math.Pow(2, octave))), Convert.ToInt32(Math.Round(time)));
            Thread.Sleep(Convert.ToInt32(Math.Round(time)));

        }
        /* -- -- -- -- -- -- -- -- -- -- */
    }
}
